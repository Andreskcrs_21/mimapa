package com.example.mimap.ui.home;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.mimap.MapsActivity;
import com.example.mimap.Miposicion;
import com.example.mimap.R;
import com.example.mimap.databinding.FragmentHomeBinding;

public class HomeFragment extends Fragment implements View.OnClickListener {

    private HomeViewModel homeViewModel;
    private FragmentHomeBinding binding;
    private Button btnUbi, btnMapa;
    private EditText txtLong;
    private EditText txtlat;
    private EditText txtalt;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);
        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        btnUbi=root.findViewById(R.id.btnUbicacion);
        txtlat=root.findViewById(R.id.txtlatitud);
        txtLong=root.findViewById(R.id.txtlongitud);
        txtalt=root.findViewById(R.id.txtaltitud);
        btnMapa=root.findViewById(R.id.btnMapa);
        btnUbi.setOnClickListener(this);
        btnMapa.setOnClickListener(this);
        miposicion();
        return root;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    public void verificar(){
        if (!txtlat.getText().toString().equals("0.0")|| !txtLong.getText().toString().equals("0.0") || !txtalt.getText().toString().equals("0.0")) {
            Intent intent = new Intent(getContext(), MapsActivity.class);
            intent.putExtra("longitud", txtLong.getText().toString());
            intent.putExtra("latitud", txtlat.getText().toString());
            intent.putExtra("altitud", txtalt.getText().toString());
            startActivity(intent);
        }else{
            Toast.makeText(getContext(),"Primero obtenga los datos de su ubicación", Toast.LENGTH_LONG).show();
        }
    }
    @Override
    public void onClick(View v) {
        if(v==btnUbi){
            miposicion();
        }
        if(v==btnMapa){
            verificar();

        }
    }
    public void miposicion(){
        if (ContextCompat.checkSelfPermission((Activity) getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
        != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions((Activity) getContext(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET}, 1000);
        }
        LocationManager objLocation=null;
        LocationListener objLocListener;

        objLocation=(LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
        objLocListener=new Miposicion();
        objLocation.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, objLocListener);

        if(objLocation.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            txtlat.setText(Miposicion.latitud+"");
            txtLong.setText(Miposicion.longitud+"");
            txtalt.setText(Miposicion.altitud+"");

        }
    }
}